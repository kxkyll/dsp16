Distributed Systems Project 2016, 
Assignment 3: Multitier architectures and the Web

Kati Kyllönen, 011539913
All work has been done by myself, no teamwork involved.

Step 1
------

    Simple calculator functionality with precedence from left to right.
    Implemented with HTML, Javascript and PHP.

    Usable in address:
    http://kxkyllon.users.cs.helsinki.fi/dsp/one.php

    Usage instructions:
    Insert numbers and operands e.g. 1+2*3-4/5 and press submit button.
    Old results can be cleared by pressing clear button.


    Files:
    one.php Frontend. Manages the user interface and parses user input
    calc.php Backend. Manages atomic calculation operations and saving to database
    clearHistory.php Backend. Manages clearing the database

    Brief explination of code functionality:
    User can insert a calculation to the textbox in web page and when the submit button is pressed
    the given input is read in as a string. The input string is first stripped so that each character is placed to a table and then collected back to a table of numbers and operators. The parser can manage negative numbers and decimal numbers (with delimiter .).
    The table of numbers and operators is iterated once and each atomic operation is sent to backend server to be calculated.
    The communication between fronend and backend is managed with XMLHttpRequest API.

    The calculations are saved to postgresql database in backend server called users.
    By default the database is cleared from old calculations when the web page one.php is loaded.
    A user can also clear the old calculations from the web page and from database by pressing the clear button.
    The database clearing is managed in file clearHistory.php

Step 2
------

Variant 1:

    Calculator as in Step 1
    Added sin plot that is created in backend using gnuplot.

    Usable in address:
    http://kxkyllon.users.cs.helsinki.fi/dsp/two.php


    Usage instructions:
    Calculator as in Step 1
    For sin plotting press button "sin plot"
    Button clear clears old calculations and plot from from web page and from database.
    Note that pressing "sin plot" button clears also old calculations to make space for the plot. 

    Files:
    two.php Frontend & Backend of sin plotting. Frontend for calculator
    calc.php As in Step 1. Backend. Manages atomic calculation operations and saving to database
    clearHistory.php As in Step 1. Backend. Manages clearing the database

    Brief explination of code functionality for sin plotting:
    When user presses "sin plot" button the hidden form is sent from frontend with get HTTP message to backend.
    Backend receives the GET request and forms a string of commands for plotting.
    Backend opens a pipe to shell and gives plotting commands to gnuplot.
    Then the pipe is closed and the read rights are given to plot image.
    As a last action the backend code return the plot image to frontend enclosed in HTML paragraph.

Variant 2:
    
    Calculator as in Step 1
    Added sin plot that is created in frontend.

    Usable in address:
    http://kxkyllon.users.cs.helsinki.fi/dsp/twotwo.html   
    
    Usage instructions:
    Calculator as in Step 1
    For sin plotting press button "Draw sin frontend"
    Button "Clear" clears old calculations and sin plot from web page and old calculations from database.

    Files:
    twotwo.html Frontend of sin calculations and plotting and calculator
    calc.php As in Step 1. Backend. Manages atomic calculation operations and saving to database
    clearHistory.php As in Step 1. Backend. Manages clearing the database

    Brief explination of frontend code functionality for sin plotting:
    When user presses "Draw sin frontend" button the javascript draw function draws first x and y axis and places the 
    -pi and pi ticks for x axis and -1 and 1 ticks for y axis. The sin wawe drawing is started from -pi as requested in the assignment
    and continued up to pi value with steps of 0.1. The corresponding y axis values for each x axis values are calculated using javascript Math.sin function. The last x axis value is forced to be exactly 3.14 as the line would otherwise not end exactly to positive pi value.
    Each x and y coordinate of the plot is scaled to look bigger in the web page. The x-scale factor is 40 and y-scale factor is 100.

Variant 3:

    Calculator as in Step 1
    Added sin plot that is calculated as atomic calculation operations in backend.

    Usable in address:
    http://kxkyllon.users.cs.helsinki.fi/dsp/twothree.html

    
    Usage instructions:
    WARNING: the calculation of the sin wawe takes time. Be patient. With Firefox the plot is drown bit by bit so it might be more user friendly to use.
    Calculator as in Step 1
    For sin plotting press button "Draw sin backend"
    Button "Clear" clears old calculations and sin plot from web page and old calculations from database.    

    Files:
    twothree.html Frontend of sin drawing and calculator
    calcSin.php Backend for doing sin calculations
    calc.php As in Step 1. Backend. Manages atomic calculation operations and saving to database
    clearHistory.php As in Step 1. Backend. Manages clearing the database

    Brief explination of the code functionality for sin plotting:
    When user presses "Draw sin backend" button the frontend code starts drawing the x and y axis to the web page canvas. 
    The frontend is using Taylor Series for getting the atomic calculation operations to get the y coordinate value for each x coordinate.
    With the Taylor Series the y value for -3.14 is calculated with following method -3.14159-(-3.14159)^3/3!+(-3.14159)^5/5!-(-3.14159)^7/7!+(-3.14159)^9/9!-(-3.14159)^11/11! +(-3.14159)^13/13! -(-3.14159)^15/15!
    this series can be continued if more preciness is needed. Each atomic operation is sent to be calculated in backend server using XMLHttpRequest API. The plotting is done by moving the "pen" in the frontend and drawing line from previous point to current point.
    With this mechanism there are a lot of calculations that are sent to backend several times... 
    
Step 3
------
    Added cache to save from sending all calculation requests to backend.
    User can change the cache size.
    Calculator works nearly as in Step 1, it only checks that if the needed calculation is already in cache it uses that result
    instead of sending the query to backend.
    Added "simplify" button that simplifies the user given calculation operation
    Sin plotting as in Step 2 Variant 3, i.e Taylor Series atomic calculations are first checked from cache and only if not found sent to backend.

    Usable in address:
    http://kxkyllon.users.cs.helsinki.fi/dsp/three.html

    
    Usage instructions:
    WARNING: the calculation of the sin wawe takes time. Be patient! With Firefox the plot is drown bit by bit so it might be more user friendly to use. Increasing cache size makes the calculations a bit faster. The impact of cache size to sent HTML messages is studied in attached plot cacheImpact.png 
    Calculator works as in Step 1
    For sin plotting press button "Draw sin backend"
    Button "Clear and Set Cache" clears old calculations and sin plot from web page and old calculations from database.    

    Files:
    three.html Frontend of sin drawing and calculator and implementing caching.
    calcSin.php Backend. As in Step 2 Variant 3 for doing sin calculations. 
    calc.php As in Step 1. Backend. Manages atomic calculation operations and saving to database
    clearHistory.php As in Step 1. Backend. Manages clearing the database

    Brief explination of the code functionality for sin plotting:
    The calculator works as earlier, but before the atomic calculation operation is sent to backend the cache is checked if the answer for the same operation exists there. If it does, the answer is retrieved from cache. If the answer can not be found from cache the calculation operation is sent to backend as in Phase 1.
    The simplify function that is fired when user presses the "simplify" button simplifies the user given calculation operation by replacing the left most calculation operation with cache value if it exists.
    The sin plotting operates in a similar manner as in Phase 2 Variant 3, but also utilizing the cache.
    The cache is a javascript object that is used in a way that a calculation is a property and calculation answer is a property value. This way the retrieval of cache value is fast. The cache size is limited and the n most recent calculations are kept in cache with help of an javascript array that holds the keys and next cache pointer that keeps track what cache key is the next one to be removed and in a way overwritten. The cache size is initialized to 100, but can be changed by giving a new value to the corresponding input field and by pressing "Clear and Set Cache" button.

