<head>
<meta charset="UTF-8">
</head>
<!--Kati Kyllönen-->
<html>
<body onLoad="clearHistory(1)" style="background-color:lightgrey;">


<p id="container"><!-- currently it's empty --></p>


<input type="submit" value="Clear" id="clearHistory" onclick="clearHistory(2);" />
</br>
<input type="text" name="textfield" id="txtInput" />
<input type="submit" value="Submit" id="sendButton" onclick="parseInput(document.getElementById('txtInput').value);" />


</br>
Result: 
</br>
<p id="result"><!-- currently it's empty --> </p>

<hr>
 
<form action="<?php $_SERVER["PHP_SELF"]?>" method="get">
  <input type="hidden" name="act" value="run">
  <input type="submit" value="Sin plot">
</form>
</br>
Result: 
</br>


</body>
</html>

<?php

//create a string of plotting commands for gnuplot
$plotCommands = "set terminal png"."\n";
$plotCommands = $plotCommands . " set out 'sinXplot.png'"."\n";
$plotCommands = $plotCommands . " set xtics -4,0.5,4"."\n";
$plotCommands = $plotCommands . " set mxtics 4"."\n";
$plotCommands = $plotCommands . " set style line 12 lc rgb '#808080'"."\n";
$plotCommands = $plotCommands . " set grid back ls 12"."\n";
$plotCommands = $plotCommands . " plot [-3.14:3.14] [-1:1] sin(x)" ."\n";

//receive the get from frontend
if (!empty($_GET['act'])) {
    //open a pipe to give shell commands
    //if connection fails exit
    if(!($gnup = popen("/usr/bin/gnuplot", "w"))){
        //echo $plotCommands;
        exit;
    }
    //write commands to pipe
    fwrite($gnup, $plotCommands);
    //close a pipe
    pclose($gnup);
    //change the plot rights to read
    chmod("sinXplot.png",0604);
    //return plot image enclosed to HTML paragraph
    echo '<p id="plot"><b><img src="http://kxkyllon.users.cs.helsinki.fi/dsp/sinXplot.png"></b></p>';
}
?>

<script type="text/javascript">

function parseInput(str) {
    //function reads the input string a user has given and sends it to function parse to be parsed to atomic calculations
    //then funcion iterates the table of calculations and sends each one to backend server to be calculated
    //and receives the results of the calculations and put's the calculations and result to the web page
    if (str.length < 3) { 
        document.getElementById("container").innerHTML = "Give at least two numbers with an operand + - * or / in between";
        return;
    } else {
        // parsedTable contains now the calculations so that first cell contains a number and second a operand third a number ...
        var parsedTable = parse(str)
        var i = 0
        var xmlhttp = new XMLHttpRequest();
        var calcResponse = parsedTable[0]
        // the backend response is managed by getting the response to the calculation
        // and getting the "history" of old calculations
        // these are updated to the web page
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                var outputTable = "<table>"
        
                responseSplittedToRows = xmlhttp.responseText.split("\n")
                for (var r = 0; r < responseSplittedToRows.length; r++) {
                    outputTable += "<tr><td>"+responseSplittedToRows[r]+"</td></tr>"
                }
                outputTable += "</table>"
                document.getElementById("result").innerHTML = outputTable;
                responseTable = xmlhttp.responseText.split("=")
                calcResponse = responseTable[responseTable.length - 1]
                i += 2

                if ( i+2 < parsedTable.length ){
                    xmlhttp.open("GET", "calc.php?arg1=" + calcResponse + "&op=" +parsedTable[i+1] +"&arg2="+parsedTable[i+2] , true); 
                    xmlhttp.send();
                }
            }
        };
        // the calculation is sent to backend server to be calculated
        if ( i+2 < parsedTable.length ){
            xmlhttp.open("GET", "calc.php?arg1=" + calcResponse + "&op=" +parsedTable[i+1] +"&arg2="+parsedTable[i+2] , true); 
            xmlhttp.send();
        }
    }
}


function parse(str) {
    //function parses input string and forms a table of calculations that contains numbers and operands (+,-,* or /)
    var inputTable = str.split("") //splits each character in the input
    var item = ""
    var iLen = inputTable.length;
    var itemTable = []
    var j = 0
    var previous = ""
    
    for (var i = 0; i < iLen; i++) {
        
        if (previous == "+" || previous == "-" || previous == "/" || previous == "*" || i == 0){
            item = item + inputTable[i]
            previous = inputTable[i]
        }else if (inputTable[i] === "+"  || inputTable[i] === "-" || inputTable[i] === "*"  || inputTable[i] === "/"  ){            
            previous = inputTable[i]
            if (inputTable[i] === "+"){
                inputTable[i] = "%2B"
            }
            if (inputTable[i] === "/"){
                inputTable[i] = "%2F"
            }
            
            if (item != ""){
                itemTable[j] = item
                item = ""
                j++
            }
        
            itemTable[j] = inputTable[i]
            j++
        }else{
            item = item + inputTable[i]
            previous = inputTable[i]
        }
    }
    if (item.length > 0){
        itemTable[j] = item
    }
    
    return itemTable

}

function clearHistory(start) {
    // clears the database from backend server and calculation history from the web page
    var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById('txtInput').value = "";
                document.getElementById("result").innerHTML = "";
                if (start == 2) {
                    document.getElementById("container").innerHTML = xmlhttp.responseText;
                    if (document.getElementById("plot").innerHTML != null) {
                          document.getElementById("plot").innerHTML = "";
                    }
                }else{
                    document.getElementById("container").innerHTML = "Give at least two numbers with an operand + - * or / in between";       
                }

            }
        };

        xmlhttp.open("GET", "clearHistory.php", true); 
        xmlhttp.send();
        
} 

</script>

